﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour
{

	public Vector2 mapSize;
	public Transform tilePrefab;

	[Range(0, 1)]
	public float outlinePercent;

	// Start is called before the first frame update
	void Start()
	{
		StartCoroutine(GenerateAsteroids());
	}

	IEnumerator GenerateAsteroids()
	{
		for (int x = 0; x < mapSize.x; x++)
		{
			for (int y = 0; y < mapSize.y; y++)
			{
				int spawnPointIndexForX = Random.Range(0, 160);
				int spawnPointIndexForY = Random.Range(0, 160);
				Vector2 tilePosition = new Vector2(-mapSize.x / 2 + 0.5f + spawnPointIndexForX, -mapSize.y / 2 + 0.5f + spawnPointIndexForY);
				Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.identity) as Transform;
				newTile.localScale = Vector2.one * (1 - outlinePercent);
				yield return new WaitForSeconds(3);
			}
		}
		
		
	}
}

