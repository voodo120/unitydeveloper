﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShipController : MonoBehaviour
{
	public int count;
	public Text scoreText;
	public Text youLoseText;

	public Rigidbody2D rb;
	public float moveRate = 5;
	public float moveAxis;
	public float turnAxis;

	public float shiftMinus;
	public float shiftPlus;

	public GameObject prefab;
	public Transform spawnPoint;
	public float rpm;

	public float secondsBetweenShots;
	public float nextPossibleShootTime;

	public Vector2 shiftAsteroidSpawn;

	public float bulletThurst = 7;

	public GameObject resetButton;

	public GameObject asteroidPrefab;

	private void Awake()
	{

		resetButton = GameObject.FindGameObjectWithTag("RestartButton");
		resetButton.gameObject.SetActive(false);
	}

	void Start()
	{
		secondsBetweenShots = 60 / rpm;
		count = 0;
		scoreText.text = "Score : " + count.ToString();
		rb = GetComponent<Rigidbody2D>();

	}
	// Update is called once per frame
	private void Update()
	{
		moveAxis = Input.GetAxis("Vertical");
		turnAxis = Input.GetAxis("Horizontal");
		//Input.GetButtonDown("Jump")  && 
		if (CanShoot())
		{
			GameObject clone = Instantiate(prefab,spawnPoint.position, transform.rotation);

			Rigidbody2D bullet = clone.GetComponent<Rigidbody2D>();

			nextPossibleShootTime = Time.time + secondsBetweenShots;

			bullet.AddRelativeForce(Vector2.up * bulletThurst ,ForceMode2D.Impulse);

			Destroy(clone,3.0f);
		}
	}

	private bool CanShoot()
	{
		bool canShoot = true;

		if (Time.time < nextPossibleShootTime)
		{
			canShoot = false;
		}

		return canShoot;
	}


	public void RestartGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		Time.timeScale = 1;
		gameObject.SetActive(true);

	}

	private void FixedUpdate()
	{
		rb.AddRelativeForce(Vector2.up * moveAxis * moveRate);
		rb.AddTorque(-turnAxis * 17);
	}

	public void setScore()
	{
		StartCoroutine("ThrowAsteroid");
		count = count + 1;
		scoreText.text = "Score : " + count.ToString();
	}

	public void setButtonActive()
	{
		resetButton.gameObject.SetActive(true);
		youLoseText.text = "YOU LOSE";
		gameObject.SetActive(false);
	}

	IEnumerator ThrowAsteroid()
	{
		yield return new WaitForSeconds(1);
		shiftMinus = Random.Range(-210, -150);
		shiftPlus = Random.Range(150, 210);
		shiftAsteroidSpawn = new Vector2(gameObject.transform.position.x + Random.Range(shiftMinus, shiftPlus), gameObject.transform.position.y + Random.Range(shiftMinus, shiftPlus));
		Instantiate(asteroidPrefab, shiftAsteroidSpawn, Quaternion.identity);
	}


}