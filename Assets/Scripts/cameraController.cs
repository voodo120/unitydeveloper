﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{

	public Transform target;

	public Vector3 offset;
	public float smooth = 0.25f;

	Vector3 currentVelocity;

    void FixedUpdate()
    {
		transform.position = Vector3.SmoothDamp(transform.position, target.position + offset, ref currentVelocity, smooth);
    }
}
