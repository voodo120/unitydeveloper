﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bulletController : MonoBehaviour
{
	public ShipController ship;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "asteroid")
		{
			ship.setScore();
		}

		if (collision.gameObject.tag == "asteroid")
		{
			Destroy(collision.gameObject);	
		}
	}

	private void Start()
	{
		ship = FindObjectOfType<ShipController>();
	}


}
