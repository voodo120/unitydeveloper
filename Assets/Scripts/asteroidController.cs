﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class asteroidController : MonoBehaviour
{

	public Vector3 rotationDirection;
	//private float smooth;
	public Rigidbody2D Asteroid;
	public float randMinus;
	public float randPlus;

	public ShipController ship;

	private void Start()
	{
		ship = FindObjectOfType<ShipController>();

		randMinus = Random.Range(-21, -12);
		randPlus = Random.Range(12, 21);
		rotationDirection = new Vector3(0, 0, 1);
		Asteroid = GetComponent<Rigidbody2D>();
		transform.localScale = new Vector2(randPlus-5, randPlus-5);
		transform.rotation = Quaternion.Euler(0, 0, Random.Range(0.0f, 360.0f));
		Asteroid.velocity = new Vector2(Random.Range(randMinus, randPlus), Random.Range(randMinus, randPlus));


	}
	/*
	 void Update()
	{
		smooth = rand *Time.deltaTime * 0.14f * Random.Range(145f, 265f);
		transform.Rotate(rand * rotationDirection * smooth);
		Asteroid.AddRelativeForce(new Vector2(0, 1)*smooth);
		
	}
	*/

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Player"){

			Time.timeScale = 0;
			ship.setButtonActive();
			
		}
		if (collision.gameObject.tag == "asteroid")
		{
			Destroy(collision.gameObject);
		}
		if (collision.gameObject.tag == "bullet")
		{
			Destroy(gameObject);
		}
	}

	

}
